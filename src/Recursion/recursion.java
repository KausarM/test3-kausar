package Recursion;

public class recursion{

  public int recursiveCount(String words[], int n){
    return recursiveCount(words, n, 0);
  }
  public int recursiveCount(String words[], int n, int index){
    if(index==words.length)
     return 0;
    boolean flag = false;
    for(int j=1;j<=words[index].length();j++){
        if(words[index].charAt(j-1)=='w'){
          if(j>=n && j%2!=0){
            flag = true;
          }
          else{
            flag = false;
            break;
          }
        }
    }
    if(flag == true){
      return 1 + recursiveCount(words, n, index + 1);
    }
    else return recursiveCount(words, n, index + 1);
  }
}