package bet;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class HTApplication extends Application {	
	public void start(Stage stage) {
		
		
		
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Heads or Tails"); 
		stage.setScene(scene); 
		
		//create the buttons
		Button heads = new Button ("Heads");
		Button tails = new Button ("Tails");
		
		//add buttons to the horizontal box
		HBox buttons = new HBox (heads,tails);
		
		//creating the textField
		Label message = new Label ("Welcome!");
		message.setPrefWidth(150);
		message.setTextFill(Color.WHITE);
		Label user = new Label ("User's Money:");
		user.setTextFill(Color.WHITE);
		user.setPrefWidth(100);
		Label money = new Label ("100");
		money.setTextFill(Color.WHITE);
		money.setPrefWidth(50);
		Label bet = new Label ("Bet: ");
		bet.setTextFill(Color.WHITE);
		TextField amount = new TextField ();
		amount.setPrefWidth(150);
		Label comments = new Label();
		comments.setTextFill(Color.WHITE);
		comments.setPrefWidth(200);
		
		
		//adding the textfields to a second horizontalbox
		HBox texts = new HBox (user, money, bet,amount);
	
		
		//adding the 2 HBox to the vertical box
		VBox page = new VBox (message, texts,buttons, comments);
		
		//adding the VBox to the root
		root.getChildren().add(page);
		
		//actions for buttons 
		HeadsTails input = new HeadsTails (0,"", 100);
		heads.setOnAction(new HTGame( money,amount,"heads", input));
		tails.setOnAction(new HTGame( money,amount,"tails", input));

		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}