package bet;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class HTGame implements EventHandler<ActionEvent> {
	
	private Label money;
	private TextField amount;
	private String choice;
	private HeadsTails input;
	private Label comments;
	
	public HTGame (Label money,TextField amount, String choice, HeadsTails input) {
		//constructor
		this.money= money;
		this.amount = amount;
		this.choice = choice;
		this.input= input;

	}
	@Override
	public void handle(ActionEvent e) {
		

		this.comments.setText(this.input.getResult(choice));
		this.money.setText(""+ this.input.getMoney());
		System.out.print(""+ this.input.getMoney());

}
}
