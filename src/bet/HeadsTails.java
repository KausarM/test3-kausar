package bet;

import java.util.Random;

import javafx.scene.control.Label;

public class HeadsTails {
	
	//object created for the game
	
	private String choice;
	private int bet;
	private int money;
	Random ranGenerator = new Random();

	public HeadsTails ( int bet, String choice, int money) {
		//constructor
		this.bet = bet;
		this.choice = choice;
		this.money = money;
	}
	
	public int getBet() {
		return this.bet;
	}

	public String getChoice() {
		return this.choice;
	}
	public int getMoney() {
		return this.money;
	}
	
	public String getResult(String choice) {
		//method to randomly do a "toss" and return result

		int heads = 0;
		int tails = 1;
		int random = ranGenerator.nextInt(2);
		String result ="";
		
		if (this.bet > this.money || this.bet == 0 ) {
			result = "Bet is not valid.";
		}
		
		else {
		if( choice.equals("heads")) {
			if (random == heads) {

				this.money += this.bet;
				result = "You won the bet and won " + this.bet;
			}
			else {

				this.money -= this.bet;
				result = "You lost the bet and lost " + this.bet;
			}
		}
		
		if( choice.equals("tails")) {
			if (random == tails) {

				this.money += this.bet;
				result = "You won the bet and won " + this.bet;
			}
			else {
				result = "You lost the bet and lost " + this.bet;
				this.money -= this.bet;
			}
			
		}}
		return result;	
	}
	
	
	
	public static void main(String[] args) {
		// testing the getResult method to make sure it works
		/*HeadsTails alpha = new HeadsTails (100,"heads",100);
		String choice = alpha.getChoice();
		System.out.println(alpha.getResult(choice));
		System.out.println(alpha.getMoney());
		System.out.println(alpha.getResult(choice));
		System.out.println(alpha.getMoney());
		System.out.println(alpha.getResult(choice));
		System.out.println(alpha.getMoney());
		System.out.println(alpha.getResult(choice));
		System.out.println(alpha.getMoney());
		System.out.println(alpha.getResult(choice));
		System.out.println(alpha.getMoney());

	*/}

	}    


